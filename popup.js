function displayUserInfo(r) {
  'use strict';
  document.getElementById('error').style.display = 'none';

  document.getElementById('uid-cell').innerHTML = r.uid;
  document.getElementById('utype-cell').innerHTML = r.utype;
  document.getElementById('ushard-cell').innerHTML = r.ushard;
  document.getElementById('uname-cell').innerHTML = r.uname;
  document.getElementById('uemail-cell').innerHTML = r.uemail;
  document.getElementById('udisabled-cell').innerHTML = r.udisabled;
  document.getElementById('uscope-cell').innerHTML = r.uscope;
  document.getElementById('usource-cell').innerHTML = r.usource;
  document.getElementById('uagent-cell').innerHTML = r.uagent;
  document.getElementById('ulogin-cell').innerHTML = r.ulogin;
  document.getElementById('uip-cell').innerHTML = r.uip;
  document.getElementById('usignup-cell').innerHTML = r.usignup;
  document.getElementById('upaying-cell').innerHTML = r.upaying;
  document.getElementById('uservices-cell').innerHTML = r.uservices;
  document.getElementById('ulimit-cell').innerHTML = r.ulimit;

  if (r.upaying === 'YES') {
    document.getElementById('upaying-cell').style.color = '#00aa00';
  } else {
    document.getElementById('upaying-cell').style.color = '#aa0000';
  }
  if (r.uagent.indexOf('MSIE 8.0') !== -1 || r.uagent.indexOf('MSIE 7.0') !== -1) {
    document.getElementById('uagent-cell').style.color = '#aa0000';
  }
  if (r.udisabled === 'YES') {
    document.getElementById('udisabled-cell').style.color = '#aa0000';
  }
}

// This part of admin was changed recently, so I have commented it out for now.

/*function displaySites(id, sites) {
  'use strict';
  document.getElementById('sites-table').innerHTML = '';
  var i;
  for (i = 0; i < sites.length; i++) {
    if (i % 2 !== 0) {
      document.getElementById('sites-table').innerHTML += '<tr class="odd"><td>' + sites[i][2] + '<br/><a href="http://' + sites[i][3] + '" target="_blank">' + sites[i][3] + '</a><br/><span>' + sites[i][1] + '</span></td><td><a class="edit-site-btn" href="http://admin.weebly.com/admin/remote_login.php?user_id=' + id + '&host=www&r=http%3A%2F%2Fwww.weebly.com%2Fweebly%2FtoSite.php%3Fsite%3D' + sites[i][1] + '" target="_blank">Edit Site</a></td></tr>';
    } else {
      document.getElementById('sites-table').innerHTML += '<tr><td>' + sites[i][2] + '<br/><a href="http://' + sites[i][3] + '" target="_blank">' + sites[i][3] + '</a><br/><span>' + sites[i][1] + '</span></td><td><a class="edit-site-btn" href="http://admin.weebly.com/admin/remote_login.php?user_id=' + id + '&host=www&r=http%3A%2F%2Fwww.weebly.com%2Fweebly%2FtoSite.php%3Fsite%3D' + sites[i][1] + '" target="_blank">Edit Site</a></td></tr>';
    }
  }
}*/

function displayLoadError(r) {
  'use strict';
  document.getElementById('user-info').style.display = 'none';
  document.getElementById('error').style.display = 'block';

  if (r.error === 'User Type') {
    document.getElementById('error').innerHTML = 'Be sure you are connected to VPN.';
  } else {
    document.getElementById('error').innerHTML = 'Unable to find ' + r.error;
  }
}

function updateTicketStats() {
  'use strict';

  chrome.extension.sendRequest({
    msg: 'getChats'
  }, function(response){
    document.getElementsByClassName('stats-value')[0].innerHTML = response.val;
  });
}

// Sends a message to background.js telling it to open the user's account within Admin for a more detailed view.
function sendOpenAdmin() {
  'use strict';
  chrome.extension.sendMessage({
    msg: 'openAdmin',
    id: document.getElementById('uid-cell').innerHTML
  });
}

// Sends a message to background.js telling it to open the user's account as they would in a new tab.
function sendLogin() {
  'use strict';
  chrome.extension.sendMessage({
    msg: 'openUserAccount',
    id: document.getElementById('uid-cell').innerHTML
  });
}

function resetStats() {
  'use strict';
  chrome.extension.sendMessage({
    msg: 'resetStats'
  });
  updateTicketStats();
}

function addChat() {
  'use strict';
  chrome.extension.sendMessage({
    msg: 'addChat',
    val: 1
  });
  updateTicketStats();
}

document.addEventListener('DOMContentLoaded', function () {
  'use strict';
  var openAdmin, loginUser, resetButton, plusButton;
  chrome.runtime.onMessage.addListener(function (message) {
    if (message.msg === 'gotInfo') {
      displayUserInfo(message);
    } else if (message.msg === 'error') {
      displayLoadError(message);
    } else if (message.msg === 'gotSites') {
      displaySites(message.uid, message.sites);
    }
  }
  );

  updateTicketStats();

  chrome.extension.sendRequest({
    msg: 'getUserInfo'
  });

  openAdmin = document.getElementById('open-admin');
  openAdmin.addEventListener('click', function () {
    sendOpenAdmin();
    window.close();
  });

  loginUser = document.getElementById('login-user');
  loginUser.addEventListener('click', function () {
    sendLogin();
    window.close();
  });

  resetButton = document.getElementById('reset-button');
  resetButton.addEventListener('click', function () {
    resetStats();
    updateTicketStats();
  });

  plusButton = document.getElementById('plus-button');
  plusButton.addEventListener('click', function () {
    addChat();
    updateTicketStats();
  });

}, false);
