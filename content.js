if (window.document.title === 'Weebly Admin') {
  var link = document.createElement('link');
  link.type = 'image/x-icon';
  link.rel = 'shortcut icon';
  link.href = 'http://www.weebly.com/favicon.ico';
  document.getElementsByTagName('head')[0].appendChild(link);
}

function getUserID() {
  'use strict';
  var allTabs = document.getElementsByClassName('tab'), userids = document.getElementsByClassName('user_id'), classList, i, j;

  try {
    for (i = 0; i < allTabs.length; i++) {
      classList = allTabs[i].className.split(/\s+/);
      for (j = 0; j < classList.length; j++) {
        if (classList[j] === 'selected') {
          return userids[i].childNodes[3].innerText;
        }
      }
    }
  } catch (e) {}
}

chrome.runtime.onMessage.addListener(
function (message, sender, sendResponse) {
  'use strict';

  // Checks a user's site for uploaded images, which will include their User ID within the URL, or pulls it straight out of ZenDesk if possible.
  if (message.msg === 'getUserID') {
    var R = document.getElementsByTagName('html')[0].innerHTML.match(/\/uploads\/([0-9]\/)+([0-9]+)\/.+\.[a-zA-Z]{3}/), userId = getUserID();
    if (userId) {
      sendResponse({
        uid: userId
      });
    } else if (R) {
      userId = R[2];
      sendResponse({
        uid: userId
      });
    } else {
      sendResponse({
        uid: 'User ID not found.'
      });
    }
  }

  // Sends the results of getInInbox & getSolved back to be added and displayed.
  if (message.msg === 'getZDStats') {
    sendResponse({
      inbox: getInInbox(),
      solved: getSolved()
    });
  }
}
);

function getSolved() {
  'use strict';
  var solved = 0;
  if (document.getElementsByClassName('stats-group')[1]) {
    solved = document.getElementsByClassName('stats-group')[1].getElementsByClassName('cell-value')[2].innerText;
  }
  return Number(solved);
}

function getInInbox() {
  'use strict';
  var inInbox;
  if (document.getElementsByClassName('fresh-count')[0]) {
    inInbox = document.getElementsByClassName('fresh-count')[0].innerText.replace(/[^\d]+/g,'');
  }
  return Number(inInbox);
}