if (!localStorage.chats) {
  localStorage.chats = 0;
}
if (!localStorage.finalStats) {
  localStorage.finalStats = 0;
}

function updateBadgeText() {
  'use strict';

  getZDStats();
  chrome.browserAction.setBadgeText({
    text: localStorage.finalStats
  });
}

function getElementsStartsWithId(html, id) {
  'use strict';
  var p = new DOMParser(),
  d = p.parseFromString(html, 'text/html'),
  children = d.getElementsByClassName('sites-table')[0].getElementsByTagName('tr'),
  elements = [],
  child,
  i,
  length;
  for (i = 0; i < children.length; i++) {
    child = children[i];
    if (child.id.substr(0, id.length) === id) {
      elements.push(child);
    }
  }
  return elements;
}

function getUserInfo(id) {
  'use strict';
  var xhr = new XMLHttpRequest(), params = 'type=data&action=user&user_id=' + id, html;
  xhr.open('POST', 'http://admin.weebly.com/admin/?userId=' + id, true);
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      html = xhr.responseText;
      console.log(html);
      // Getting the User Type
      var typeLoc = html.indexOf('Type</div>'), userTypeStart, typeLoc2, userType,
      shardLoc, userShardStart, shardLoc2, userShard,
      nameLoc, userNameStart, nameLoc2, userName,
      emailLoc, userEmailStart, emailLoc2, userEmail,
      disabledLoc, userDisabledStart, disabledLoc2, userDisabled,
      scopeLoc, userScopeStart, scopeLoc2, userScope,
      sourceLoc, userSourceStart, sourceLoc2, userSource,
      agentLoc, userAgentStart, agentLoc2, userAgent,
      loginLoc, userLoginStart, loginLoc2, userLogin,
      ipLoc, userIPStart, ipLoc2, userIP,
      signupLoc, userSignupStart, signupLoc2, userSignup,
      payingLoc, userPayingStart, payingLoc2, userPaying,
      servicesLoc, userServicesStart, servicesLoc2, userServices,
      limitLoc, userLimitStart, limitLoc2, userLimit;
      if (typeLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'User Type'
        });
        return;
      }
      userTypeStart = html.substring(typeLoc + 29);
      typeLoc2 = userTypeStart.indexOf('</div>');
      userType = userTypeStart.substring(0, typeLoc2);

      // Getting the User Shard
      shardLoc = html.indexOf('Shard</div>');
      if (shardLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'User Shard'
        });
        return;
      }
      userShardStart = html.substring(shardLoc + 30);
      shardLoc2 = userShardStart.indexOf('</div>');
      userShard = userShardStart.substring(0, shardLoc2);

      // Getting the User Name
      nameLoc = html.indexOf("title:'Change Username', label:'Username', updateField:'user-username', type:'user', action:'changeUsername'}); return false;\">");
      if (nameLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'User Name'
        });
        return;
      }
      userNameStart = html.substring(nameLoc + 127);
      nameLoc2 = userNameStart.indexOf('</a>');
      userName = userNameStart.substring(0, nameLoc2);

      // Getting the User Email
      emailLoc = html.indexOf("title:'Change Email', label:'Email', updateField:'user-email', type:'user', action:'changeEmail'}); return false;\">");
      if (emailLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'User Email'
        });
        return;
      }
      userEmailStart = html.substring(emailLoc + 115);
      emailLoc2 = userEmailStart.indexOf('</a>');
      userEmail = userEmailStart.substring(0, emailLoc2);

      // Getting the Disabled
      disabledLoc = html.indexOf('deleted">');
      if (disabledLoc === -1) {
        userDisabled = 'NO';
      } else {
        userDisabled = 'YES';
      }

      // Getting the Billing Scope
      scopeLoc = html.indexOf('Billing Plan Scope</div>');
      if (scopeLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'Billing Plan Scope'
        });
        return;
      }
      userScopeStart = html.substring(scopeLoc + 43);
      scopeLoc2 = userScopeStart.indexOf('</div>');
      userScope = userScopeStart.substring(0, scopeLoc2);

      // Getting the Source
      sourceLoc = html.indexOf("action:'changeSource', }); return false;\">");
      if (sourceLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'User Source'
        });
        return;
      }
      userSourceStart = html.substring(sourceLoc + 42);
      sourceLoc2 = userSourceStart.indexOf('</div>');
      userSource = userSourceStart.substring(0, sourceLoc2);

      // Getting the User Agent
      agentLoc = html.indexOf('User Agent</div>');
      if (agentLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'User Agent'
        });
        return;
      }
      userAgentStart = html.substring(agentLoc + 35);
      agentLoc2 = userAgentStart.indexOf('</div>');
      userAgent = userAgentStart.substring(0, agentLoc2);

      // Getting the Last Login
      loginLoc = html.indexOf('Last Login</div>');
      if (loginLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'Last Login'
        });
        return;
      }
      userLoginStart = html.substring(loginLoc + 35);
      loginLoc2 = userLoginStart.indexOf('</div>');
      userLogin = userLoginStart.substring(0, loginLoc2);

      // Getting the Last IP
      ipLoc = html.indexOf('Last IP</div>');
      if (ipLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'Last IP'
        });
        return;
      }
      userIPStart = html.substring(ipLoc + 32);
      ipLoc2 = userIPStart.indexOf('</div>');
      userIP = userIPStart.substring(0, ipLoc2);

      // Getting the Signup Date
      signupLoc = html.indexOf('Signup Date</div>');
      if (signupLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'Signup Date'
        });
        return;
      }
      userSignupStart = html.substring(signupLoc + 36);
      signupLoc2 = userSignupStart.indexOf('</div>');
      userSignup = userSignupStart.substring(0, signupLoc2);

      // Getting the Paying User
      payingLoc = html.indexOf('; color:white;\">');
      if (payingLoc === -1) {
        userPaying = 'NO';
      } else {
        userPayingStart = html.substring(payingLoc + 16);
        payingLoc2 = userPayingStart.indexOf('</div>');
        userPaying = userPayingStart.substring(0, payingLoc2);
      }

      // Getting the Active Services
      servicesLoc = html.indexOf('Active Services</div>');
      if (servicesLoc === -1) {
        userServices = '';
      } else {
        userServicesStart = html.substring(servicesLoc + 21);
        servicesLoc2 = userServicesStart.indexOf('</div>');
        userServices = userServicesStart.substring(0, servicesLoc2);
      }

      // Getting the Site Limit
      limitLoc = html.indexOf("updateField:'user-site-limit'})\">");
      if (limitLoc === -1) {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'Site Limit'
        });
        return;
      }
      userLimitStart = html.substring(limitLoc + 33);
      limitLoc2 = userLimitStart.indexOf('</a>');
      userLimit = userLimitStart.substring(0, limitLoc2);

      // Sending the data back to popup
      chrome.extension.sendMessage({
        msg: 'gotInfo',
        uid: id,
        utype: userType,
        ushard: userShard,
        uname: userName,
        uemail: userEmail,
        udisabled: userDisabled,
        uscope: userScope,
        usource: userSource,
        uagent: userAgent,
        ulogin: userLogin,
        uip: userIP,
        usignup: userSignup,
        upaying: userPaying,
        uservices: userServices,
        ulimit: userLimit
      });
      //checkForMulti(userIP);
    }
  };
  xhr.send(params);
}

// Unfortunately, this is no longer useful, but I am leaving it for now.
function getUserSites(id) {
  'use strict';
  var xhr = new XMLHttpRequest(), params = 'type=data&action=sites&user_id=' + id;
  xhr.open('POST', 'http://admin.weebly.com/admin/?userId=' + id, true);
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      var html = xhr.responseText, siteData = [], siteRows = getElementsStartsWithId(html, 'site-row'), h, i, j;

      for (h = 0; h < siteRows.length; h++) {
        if (siteRows[h].className.substring(0, 7) !== 'deleted') {
          siteData.push(siteRows[h].innerText.replace(/\n/g, "/").split("/"));
        }
      }

      for (i = 0; i < siteData.length; i++) {
        for (j = 0; j < siteData[i].length; j++) {
          siteData[i][j] = siteData[i][j].trim();
        }
      }

      // Sending the data back to popup
      chrome.extension.sendMessage({
        msg: 'gotSites',
        uid: id,
        sites: siteData
      });
    }
  };
  xhr.send(params);
}

function getUserID() {
  'use strict';
  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {
      msg: 'getUserID'
    }, function (response) {
      if (response.uid && response.uid === 'User ID not found.') {
        chrome.extension.sendMessage({
          msg: 'error',
          error: 'User ID'
        });
      } else {
        getUserInfo(response.uid);
        getUserSites(response.uid);
      }
    });
  });
}

function getZDStats() {
  'use strict';
  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {
      msg: 'getZDStats'
    }, function (response) {
      var zd = Number(response.inbox) + Number(response.solved);
      localStorage.finalStats = String((Number(localStorage.chats) * 1.3) + zd);
      updateBadgeText();
    });
  });
}

updateBadgeText();

chrome.extension.onRequest.addListener(
function (request, sender, sendResponse) {
  'use strict';
  if (request.msg === 'getUserInfo') {
    getUserID();
  }

  if (request.msg === 'getChats') {
    sendResponse({val: localStorage.chats});
    updateBadgeText();
  }
}
);

chrome.extension.onMessage.addListener(
function (message, sender, sendResponse) {
  'use strict';

  if (message.msg === 'idFound') {
    chrome.browserAction.setBadgeBackgroundColor({
      color: '#cc0000'
    });
  }

  if (message.msg === 'idNotFound') {
    chrome.browserAction.setBadgeBackgroundColor({
      color: '#cc0000'
    });
  }

  if (message.msg === 'openAdmin') {
    chrome.tabs.query({
      pinned: true,
      title: 'Weebly Support Tool'
    }, function (t) {
      if (t.length > 0) {
        chrome.tabs.update(t[0].id, {
          url: 'http://admin-beta6.weebly.com/support/#user/' + message.id,
          active: true
        });
      } else {
        chrome.tabs.create({
          url: 'http://admin-beta6.weebly.com/support/#user/' + message.id,
          pinned: true,
          active: true
        });
      }
    });
  }

  if (message.msg === 'openUserAccount') {
    chrome.tabs.create({
      url: 'http://admin.weebly.com/admin/remote_login.php?user_id=' + message.id + '&host=www',
      active: true
    });
  }

  if (message.msg === 'addChat') {
    var chats = Number(localStorage.chats);
    chats = chats + message.val;
    localStorage.chats = chats;
    sendResponse({val: localStorage.chats});
  }

  if (message.msg === 'resetStats') {
    localStorage.chats = '0';
  }
}
);
